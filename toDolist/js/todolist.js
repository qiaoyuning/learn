$(function () {
    //读取本地存储的数据
    let getDate = function () {
        let data = localStorage.getItem("todolist");
        if (data !== null) {
            return JSON.parse(data);
        }
        else {
            return [];
        }

    }

    //将数据存储到本地
    let saveDate = function (data) {
        localStorage.setItem("todolist", JSON.stringify(data));
    }

    //渲染数据
    let load = function () {
        let data = getDate();
        //遍历之前清空
        $("ol,ul").empty();
        let todoCount = 0; //正在进行的任务数量
        let doneCount = 0; //已经完成的数量
        //遍历这个数据
        $.each(data, function (i, n) {
            //判断是否完成
            if (n.done) {
                $("ul").prepend("<li><input type = 'checkbox' checked = 'checked'><p>" + n.title + "</p><a href='javascript:;' id=" + i + "></a></li>");
                doneCount++;
            } else {
                $("ol").prepend("<li><input type = 'checkbox'><p>" + n.title + "</p><a href='javascript:;' id=" + i + "></a></li>");
                todoCount++;
            }
        });
        $("#todocount").text(todoCount);
        $("#donecount").text(doneCount);
    }
    load();


    // 删除按钮
    $("ol,ul").on("click", "a", function () {
        let data = getDate();
        let index = $(this).attr("id");
        data.splice(index, 1);
        // //删除当前元素
        // data.splice($(this).parent().index(), 1);
        saveDate(data);
        load();
    })


    // 完成按钮
    $("ol,ul").on("click", "input", function () {
        let data = getDate();
        //获取当前元素的索引
        let index = $(this).siblings("a").attr("id");
        //修改当前元素的done属性
        data[index].done = $(this).prop("checked");
        //保存数据到本地
        saveDate(data);
        load();
    })


    //回车添加
    $("#title").on("keydown", function (e) {
        if (e.keyCode === 13) {
            let local = getDate();
            local.push({
                title: $("#title").val(),
                done: false,
            });
            saveDate(local);
            //每次输入后重新加载渲染
            load();
            $("#title").val("");
        }
    })



})