// 导入fs模块来操作文件
const fs = require('fs');

//使用fs。readFile方法读取文件内容
fs.readFile('1.text', 'utf-8', (err, data) => {
    if (err) {
       return console.log('读取文件失败' + err.message);
    } else {
        console.log(data);
    }
});