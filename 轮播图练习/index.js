// 鼠标经过 显示左右按钮
window.addEventListener('load', function () {
    let left = document.querySelector(".left");
    let right = document.querySelector(".right");
    let div = document.querySelector("div");
    let ul = document.querySelector("ul");

    div.addEventListener("mouseover", function () {
        left.style.display = "block";
        right.style.display = "block";
        clearInterval(timer); // 清除定时器
        timer = null;
    });
    div.addEventListener("mouseleave", function () {
        left.style.display = "none";
        right.style.display = "none";
        timer = setInterval(function () {
            right.click()
        }, 2000);
    });
    // 定时器的添加
    let timer = setInterval(function () {
        right.click()
    }, 2000)
    // 动态生成小圆点
    let ol = document.querySelector("ol");
    for (let i = 0; i < ul.children.length; i++) {
        let li = document.createElement("li");
        //给li设置索引号
        li.setAttribute("index", i);
        ol.appendChild(li);
        //小圆圈的排他思想
        li.addEventListener("click", function () {
            for (let j = 0; j < ol.children.length; j++) {
                ol.children[j].className = "";
            }
            this.className = "current";

            //点击小圆圈切换图片
            let index = this.getAttribute("index");
            animate(ul, -index * 721);
            // 解决bug 当我们点击小圆圈切换图片，再点击右侧按钮，图片不会按顺序跳转
            num = index;
            circle = index;
        })
    }

    // 把ol里的第一个小li设置成current   选中第一个小圆点
    ol.children[0].className = "current";

    //克隆第一个li
    let firstLi = ul.children[0].cloneNode(true);
    ul.appendChild(firstLi);
    // 右侧按钮添加点击事件
    let num = 0;
    // 控制小圆圈
    let circle = 0;
    let flag = true;

    right.addEventListener("click", function () {
        //右侧点击按钮添加节流阀
        if (flag) {
            flag = false;
            if (num == ul.children.length - 1) {
                num = 0;
                ul.style.left = 0;
            }
            num++;
            animate(ul, -num * 721, function () {
                flag = true;
            });
            circle++;
            // 让小圆圈跟着按钮走
            for (let i = 0; i < ol.children.length; i++) {
                ol.children[i].className = "";
            }
            // 通过判断 确保小圆圈的位置
            if (circle == ol.children.length) {
                circle = 0;
            }
            ol.children[circle].className = "current";
        }
    })

    // 左侧按钮添加点击事件
    left.addEventListener("click", function () {
        //左侧点击按钮添加节流阀
        if (flag) {
            flag = false;
            if (num == 0) {
                num = ul.children.length - 1;
                ul.style.left = -num * 721 + "px";
            }
            num--;
            animate(ul, -num * 721, function () {
                flag = true;
            });
            circle--;
            // 让小圆圈跟着按钮走
            for (let i = 0; i < ol.children.length; i++) {
                ol.children[i].className = "";
            }
            // 通过判断 确保小圆圈的位置
            if (circle < 0) {
                circle = ol.children.length - 1;
            }
            ol.children[circle].className = "current";
        }
    })

})