const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false, // 关闭警告
  chainWebpack: config => {
    config.optimization.clear()
  },
})
