// 导入http模块
const http = require('http');
// 创建web服务实例
const server = http.createServer();
// 监听请求事件
server.on('request', (req, res) => {
    console.log(req.url);
    res.end('Hello World');
});
// 启动服务器
server.listen(80, () => {
    console.log('服务器启动成功，请访问：http://127.0.0.1:80/');
})