//简单函数的分装
let animate = function (obj, target, callback) {
    //当我们不停的点击时，盒子会越来越快，这是因为定时器不断开启
    //解决办法 只让一个定时器运行
    clearInterval(obj.timer);

    obj.timer = setInterval(function () {
        let step = (target - obj.offsetLeft) / 10
        step = step > 0 ? Math.ceil(step) : Math.floor(step)
        if (obj.offsetLeft == target) {
            //停止动画 本质是清除定时器
            clearInterval(obj.timer);
            // 回调函数写到定时器结束里面
            if (callback) {
                callback();
            }
        }
        //缓动动画 每次步长减小
        obj.style.left = obj.offsetLeft + step + 'px';

    }, 30)
}