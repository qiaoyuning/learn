window.addEventListener('load', function () {
    let username = document.getElementById("Username");
    let email = document.getElementById("Email");
    let password1 = document.getElementById("Password1");
    let password2 = document.getElementById("Password2");

    // 判断用户名
    const verifyUsername = function verifyUsername(event) {
        const element = event.target || event.srcElement;
        if (!/^\w{3,}$/.test(username.value)) {
            element.nextElementSibling.innerHTML = "username must be min 3 char";
            element.style.border = "red solid 2px";
            btn.onclick = function () {
                this.className = "btn";
            }
            return false;

        }
        else {
            element.style.border = "green solid 2px";
            element.nextElementSibling.innerHTML = "";
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
            return true;
        }
    }
    username.addEventListener("change", verifyUsername);

    // 判断邮箱
    // const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    const verifyEmail = function verifyEmail() {
        if (!/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(email.value)) {
            this.nextElementSibling.innerHTML = "Email is not valid";
            this.style.border = "red solid 2px";
            btn.onclick = function () {
                this.className = "btn";
            }
            return false;
        }
        else {
            this.style.border = "green solid 2px";
            this.nextElementSibling.innerHTML = "";
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
            return true;
        }
    }
    email.addEventListener("change", verifyEmail);

    //判断第一次输入密码
    const verifyPassword1 = function verifyPassword1(event) {
        const element = event.target || event.srcElement;
        if (!/^\w{5,}$/.test(password1.value)) {
            element.nextElementSibling.innerHTML = "password must be min 5 char";
            element.style.border = "red solid 2px";
            // 在判断密码不合格 点击按钮的时候，将边框和背景颜色设置为蓝色
            btn.onclick = function () {
                this.className = "btn";
            }
            return false;
        }
        else {
            element.style.border = "green solid 2px";
            element.nextElementSibling.innerHTML = "";
            // 清除判断密码不合格时 点击按钮改变的边框和背景颜色
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
            return true;
        }
    }
    password1.addEventListener("change", verifyPassword1);

    // 判断第二次输入密码
    const verifyPassword2 = function verifyPassword2(event) {
        const element = event.target || event.srcElement;
        if (password2.value !== password1.value) {
            element.nextElementSibling.innerHTML = "Please confirm your password";
            element.style.border = "red solid 2px";
            btn.onclick = function () {
                this.className = "btn";
            }
            return false;
        } else {

            element.style.border = "green solid 2px";
            element.nextElementSibling.innerHTML = "";
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
            return true;
        }
    }
    password2.addEventListener("change", verifyPassword2);

    // 提交按钮进行提交前的判断
    let form = document.querySelector('form');
    let btn = document.querySelector('button');
    form.addEventListener("sumbit", function (e) {
        if (!verifyUsername()) {
            // 判断用户名不合格时，阻止默认行为
            e.preventDefault();
            // 判断用户名不合格时，点击按钮改变的边框和背景颜色
            btn.onclick = function () {
                this.className = "btn";
            }
        }
        // 判断用户名合格时，恢复原本边框和背景颜色
        else {
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
        }
        if (!verifyEmail()) {
            e.preventDefault();
            btn.onclick = function () {
                this.className = "btn";
            }
        } else {
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
        }
        if (!verifyPassword1()) {
            e.preventDefault();
            btn.onclick = function () {
                this.className = "btn";
            }
        } else {
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";

            }
        }
        if (!verifyPassword2()) {
            e.preventDefault();
            btn.onclick = function () {
                this.className = "btn";
            }
        } else {
            btn.onclick = function () {
                this.style.border = "0";
                this.style.background = "#1e76f0";
            }
        }

    })

})